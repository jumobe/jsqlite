// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#ifndef CHAT_MSG_H
#define CHAT_MSG_H

#include "entity.h"

class chat_msg: public entity
{
  
public:

  // ATRIBUTES
  std::chrono::time_point<std::chrono::system_clock> msg_date;
  std::string room;
  std::string user;
  std::string msg;
  std::string msg_raw;

  // METHODS
  chat_msg(std::map<std::string, std::string>);  
  ~chat_msg();
  void print();

  // METHODS OVERRIDE
  void from_map(std::map<std::string, std::string>) override;
  const std::string to_string() override;
};

#endif
