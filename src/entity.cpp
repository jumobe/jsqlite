// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#include "entity.h"
#include <iomanip>
#include <sstream>

/**
 * Get string from date.
 */
std::string entity::date_str(
  std::chrono::time_point<std::chrono::system_clock> d)
{
  std::time_t t = std::chrono::system_clock::to_time_t(d);
  std::string str_dt(ctime(&t));
  return str_dt.substr(0, str_dt.length() - 1);
}

/**
 * Get date from string.
 */
std::chrono::time_point<std::chrono::system_clock> entity::date_from_str(
  std::string d)
{
  std::tm t;
  std::istringstream ss(d);
  ss >> std::get_time(&t, "%Y-%m-%d %H:%M:%S");
  return std::chrono::system_clock::from_time_t(std::mktime(&t)); 
}

