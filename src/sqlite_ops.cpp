// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#include "sqlite_ops.h"

#include <cassert>
#include <iostream>
#include <regex>
#include <sstream>

sqlite_ops::sqlite_ops(const std::string db_path)
{  
  if( sqlite3_open(db_path.c_str(), &db) )
  {
    std::stringstream ss;
    ss << "Can't open database: ["
       << sqlite3_errmsg(db)
       << "]";
    throw std::runtime_error(ss.str());
  }
  else
  {
    std::cout << "Opened database: " << db_path << std::endl;
  }
}

sqlite_ops::~sqlite_ops()
{
  std::cout << "Close connection" << std::endl;
  sqlite3_close(db);
}

int sqlite_ops::callback(void *resp,
                         int vc,
                         char **values,
                         char **col_names)
{
  
  std::map<std::string, std::string> row;
    
  for(int i = 0; i < vc; i++)
  {
    row[col_names[i]] = (values[i] ? values[i] : "NULL");
  }

  std::vector<std::map<std::string, std::string>> *rows = 
     reinterpret_cast<std::vector<std::map<std::string, std::string>> *>(resp);

  rows->push_back(row);
  
  return 0;
}

const std::string sqlite_ops::exec(const std::string sql, void *rows)
{
  char *err_msg = 0;  
  
  if( sqlite3_exec(db, sql.c_str(),
                   sqlite_ops::callback,
                   rows,
                   &err_msg) != SQLITE_OK )
  {    
    return std::string(err_msg);
  }

  return SQLITE_OPS_OK;
}

void sqlite_ops::create_table(const std::string sql)
{
  const std::string re = this->exec(sql);
  
  if( re != SQLITE_OPS_OK )
  {
    if (!std::regex_match(re,
                          std::regex("table [[:alnum:]]+ already exists")))
    {
      throw std::runtime_error(re);
    }
  }
  else
  {
    std::cout <<  "Table created successfully\n";
  }
}


void sqlite_ops::save(const std::string table,
                      const std::map<std::string, std::string> values)
{
  std::stringstream p0, p1;
  p0 << "insert into "
     << table
     << "(";
  for(auto v : values)
  {
    p0 << v.first;
    p1 << "'" << v.second << "'";
    
    if (v.first != values.rbegin()->first)
    {
      p0 << ", ";
      p1 << ", ";
    }
    else
    {
      p0 << ") values (";
      p1 << ");";
    }
  }
  p0 << p1.str();
  std::cout << p0.str() << std::endl;

  const std::string re = this->exec(p0.str());
   
  if( re != SQLITE_OPS_OK )
  {
    if(std::regex_match(re, std::regex("UNIQUE constraint failed: (.+)")))
    {
      this->update(table, values);
    }
    else
    {
      throw std::runtime_error(re);
    }
  }
}


void sqlite_ops::update(const std::string table,
                        const std::map<std::string, std::string> values)
{
  std::cout << "update" << std::endl;
  assert(table != "");
  assert(values.size() > 0);
}


std::vector<std::map<std::string, std::string>>
sqlite_ops::find_all( const std::string table )
{
  std::vector<std::map<std::string, std::string>> rows;

  this->exec("select * from " + table, &rows);

  return rows;
}
