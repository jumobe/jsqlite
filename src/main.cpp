// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#include <cassert>

#include "sqlite_ops.h"
#include "chat_msg.h"
#include <iostream>
#include <memory>

int main(int argc, char** argv)
{
  assert(argc > 1);
  std::unique_ptr<sqlite_ops> so = std::make_unique<sqlite_ops>(argv[1]);

  std::string sql = "create table chat_msg(" \
    "msg_date datetime default current_timestamp not null," \
    "room     text                               not null," \
    "user     text                               not null," \
    "msg      text, " \
    "msg_raw  text);";

  try
  {
    so->create_table(sql);
  }
  catch(std::runtime_error e)
  {
    std::cout << "Create table exception: "
              << e.what()
              << std::endl;
  }

  // so->save("chat_msg",
  //          {{"room", "test"},
  //            {"user", "juliano"},
  //            {"msg", "test msg"}});
  
  std::vector<std::map<std::string, std::string>> rows =
    so->find_all("chat_msg");

  std::vector<chat_msg> chat_msgs;

  for( auto r: rows )
  {
    // chat_msg cm(r);
    chat_msgs.push_back(chat_msg(r));
  }

  for(auto cm: chat_msgs)
  {
    cm.print();
  }
  
  return 0;
}
