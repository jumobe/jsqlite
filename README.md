# jsqlite

### Infos from database

-   Show the structure of a table:

    ```shell
    $ sqlite3 database.db
    sqlite> .tables
    sqlite> .schema table_name
    sqlite> .header on
    sqlite> .mode column
    sqlite> pragma table_info('table_name');
    ```