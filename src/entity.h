// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#ifndef ENTITY_H
#define ENTITY_H

#include <chrono>
#include <map>
#include <string>

class entity
{
public:  
  virtual void from_map(std::map<std::string, std::string>) = 0;
  virtual const std::string to_string() = 0;

protected:
  std::string date_str(std::chrono::time_point<std::chrono::system_clock>);
  std::chrono::time_point<std::chrono::system_clock> date_from_str(std::string);
};

#endif
