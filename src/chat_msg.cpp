// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#include "chat_msg.h"
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>

void chat_msg::from_map(std::map<std::string, std::string> row)
{
  this->room     = row["room"];
  this->user     = row["user"];
  this->msg      = row["msg"];
  this->msg_raw  = row["msg_raw"];
  this->msg_date = this->date_from_str(row["msg_date"]);
}

const std::string chat_msg::to_string()
{
  std::stringstream ss;
  ss << "chat_msg={room["
     << this->room
     << "], user["
     << this->user
     << "], msg["
     << this->msg
     << "], msg_date["
     << this->date_str(this->msg_date)
     << "]}";

  return ss.str();
}

chat_msg::chat_msg(std::map<std::string, std::string> row)
{  
  this->from_map(row); 
}

chat_msg::~chat_msg()
{
}

/**
 * Print attributes.
 */
void chat_msg::print()
{  
  std::cout << this->to_string()
            << std::endl;
}

