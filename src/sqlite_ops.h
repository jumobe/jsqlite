// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#ifndef SQLITE_OPS_H
#define SQLITE_OPS_H

#include <sqlite3.h>
#include <map>
#include <string>
#include <vector>

#define SQLITE_OPS_OK "ok"

class sqlite_ops
{
  
public:
  sqlite_ops(const std::string);
  ~sqlite_ops();
  void create_table(const std::string);
  void save(const std::string, const std::map<std::string, std::string>);
  std::vector<std::map<std::string, std::string>> find_all(const std::string);
  
private:
  sqlite3 *db;
  const std::string exec(const std::string, void *rows = 0);
  void update(const std::string, const std::map<std::string, std::string>);
  static int callback(void *, int, char **, char **);
};

#endif
